{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Hyperparameter Tuning\n",
    "\n",
    "In this notebook, we aim to improve the random forest model we trained in the previous notebook to predict whether a patient will require additional care by optimising hyperparameter values. The notebook is split into several sections:\n",
    "\n",
    "0. Setup\n",
    "1. Introduction to random forest hyperparameters\n",
    "2. Exploring the effect of varying individual hyperparameter values\n",
    "3. Systematically optimising multiple hyperparameter values to obtain a final model\n",
    "4. Extension to a support vector machine model\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 0. Setup"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we do anything else, we have to make sure that the libraries we want to use are both installed on our machine and imported into this notebook. We have installed the required modules in this training environment for you."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next import some modules: [Matplotlib](https://matplotlib.org/stable/users/index) for data visualisation, [pandas](https://pandas.pydata.org/pandas-docs/stable/user_guide/index.html) for working with DataFrames and [scikit-learn](https://scikit-learn.org/stable/user_guide.html) for machine learning:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "from sklearn.ensemble import RandomForestClassifier\n",
    "import sklearn.metrics as metrics\n",
    "from sklearn.model_selection import GridSearchCV, train_test_split"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We next load our data and split by columns into the features, `X`, and target, `y`, we aim to correctly predict:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv('/mnt/materials/Hartree/ds/data/lccg_data/classification_data.csv')\n",
    "X = df.drop(columns = ['Outcome'])\n",
    "y = df['Outcome']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We next isolate a subset of the data, `X_test` and `y_test`, to use to eventually test our final model. Ths allows us to determine how well our model may be able to generalise to unseen data. By avoiding using this data during model training and hyperparameter selection, we can avoid having information about this data ['leak'](https://scikit-learn.org/stable/common_pitfalls.html#data-leakage) into our model, potentially making our model appear to perform better than it actually does:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Introduction to random forest hyperparameters\n",
    "\n",
    "A random forest classifier is an ensemble of decision trees (base estimators) that each make a prediction for the class of an observation. This high level overview gives rise to a number of considerations concerning how an algorithm could implement this, such as:\n",
    "\n",
    "- How many trees would it use?\n",
    "- What data would be used to train each tree?\n",
    "- How should it decide when to split a particular node within a particular tree?\n",
    "- How should it combine the predictions of each decision tree to reach an overall prediction?\n",
    "- Should some classes within the training data have greater weight than others?\n",
    "\n",
    "Many of these considerations are addressed by the hyperparameters of a random forest classifier model. Recall, our definition that hyperparameters are parameters whose values are used to control the learning process. They are specificed in advance of, rather than being found during, model training. In the exercise in the last session, we build a random forest classifier using sci-kit-learn:\n",
    "\n",
    "```\n",
    "clf = RandomForestClassifier()\n",
    "```\n",
    "\n",
    "Since we didn't specify any parameters when instantiating this estimator, the default values of each parameter was used. We can look to [scikit learn's documentation](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html) to examine these parameters and their default values:\n",
    "\n",
    "```\n",
    "class sklearn.ensemble.RandomForestClassifier(n_estimators=100, *, criterion='gini',\n",
    "max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0,\n",
    "max_features='auto', max_leaf_nodes=None, min_impurity_decrease=0.0, bootstrap=True,\n",
    "oob_score=False, n_jobs=None, random_state=None, verbose=0, warm_start=False,\n",
    "class_weight=None, ccp_alpha=0.0, max_samples=None)\n",
    "```\n",
    "\n",
    "When training a model, instead of just using these default values, we may wish to choose hyperparameters that are optimal in some sense. However, to do this, we need to first consider how we define an 'optimal' model:\n",
    "\n",
    "- Prediction ability: we likely want to choose hyperparameter values that yield good accuracy, or some other metric based on the prediction that our model makes e.g., precision, recall, f1 score. It is important to also consider the value of these metrics on unseen data to ensure the model is not simply overfitting to the data it was trained on.\n",
    "- Computing resources: we may also wish to choose hyperparameters with the computing resources we will have available for model training and predictions in mind. For example, training an ensemble method, such as a random forest, using the entire training data for each base estimator, instead of bootstrapping subsets of the training data, may significantly increase model training time.\n",
    "\n",
    "## 2. Exploring the effect of varying individual hyperparameter values\n",
    "\n",
    "### 2.0 Setup\n",
    "\n",
    "To explore the effect of varying individual hyperparameter values, we will train a series of models with the value of one hyperparameter varying and the rest held fixed, and plot the accuracy of these models on both the data they were trained on and some unseen data. To avoid using our isolated test data as unseen data, we will further divide `X_train` and `y_train` into two subsets. The first subset, `X_training` and `y_training`, will be used for training while the second subset, `X_val` and `y_val`, will provide us with some unseen data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_training, X_val, y_training, y_val = train_test_split(X_train, y_train, test_size=0.2, random_state=42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the following cell so we can use the `explore_random_forest_hyperparameters` function to explore how the number of estimators, maximum tree depth and maximum samples hyperparameters affect the accuracy of a random forest model. Understanding how the function works is not required. However, if you're interested, you can experiment with using [parameters](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html) (provided they take numerical values) and [scoring metrics](https://scikit-learn.org/0.22/modules/model_evaluation.html#common-cases-predefined-values) (provided they are suitable for evaluating random forest classification models) other than those in the examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def explore_random_forest_hyperparameters(hyperparameter, hyperparameter_values, scoring='accuracy'):\n",
    "\n",
    "    param_grid = [{hyperparameter: hyperparameter_value} for hyperparameter_value in hyperparameter_values]\n",
    "    score_training_data = []\n",
    "    score_val_data = []\n",
    "\n",
    "    for i, _ in enumerate(hyperparameter_values):\n",
    "        clf = RandomForestClassifier(random_state=42, **param_grid[i])\n",
    "        clf.fit(X_training, y_training)\n",
    "        scorer = metrics.get_scorer(scoring)\n",
    "        score_training_data.append(scorer(clf, X_training, y_training))\n",
    "        score_val_data.append(scorer(clf, X_val, y_val))\n",
    "\n",
    "    plt.plot(hyperparameter_values, score_training_data)\n",
    "    plt.plot(hyperparameter_values, score_val_data)\n",
    "    plt.legend(['Training Data', 'Validation Data'])\n",
    "    plt.xlabel('Hyperparameter Value')\n",
    "    plt.ylabel('Score')\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 Number of estimators (trees)\n",
    "\n",
    "We can use the `explore_random_forest_hyperparameters` function to explore the effect of varying the number of estimators, trees, on the accuracy of trained models on the our training data, `X_training` and `y_training`, and unseen data, `X_val` and `y_val`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "explore_random_forest_hyperparameters(\n",
    "    hyperparameter=\"n_estimators\",\n",
    "    hyperparameter_values = range(1,51,1)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The graph suggests that initially increasing the estimators, trees, improves the accuracy of model on both datasets. However, after this initial increase, both plateau off suggesting it is not beneficial to increase the number of estimators, trees, further.\n",
    "\n",
    "### 2.2 Maximum tree depth\n",
    "\n",
    "Can you use the `explore_random_forest_hyperparameters` function to explore the effect of varying the value of the maximum tree depth hyperparameter, `max_depth`, on the accuracy of the model?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code goes here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "    <summary>Hint:</summary>\n",
    "        <br><code>explore_random_forest_hyperparameters(hyperparameter=\"max_depth\", hyperparameter_values = range(1,26,1))</code> <br><br>\n",
    "        The graph shows it is initially beneficial to increase the the maximum tree depth as it improves the accuracy of the model on both the training and test data. However, both lines eventually plateau, so further increasing the maximum tree depth is not beneficial. Notice the line for the validation data plataeus earlier than the training data suggesting that the model may be overfitting to the training data in this region.\n",
    "</details>\n",
    "\n",
    "### 2.3 Maximum samples\n",
    "\n",
    "The maximum samples hyperparameter, `max_samples`, controls how many samples from the training data are used to train each base estimator i.e., decision tree:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "explore_random_forest_hyperparameters(\n",
    "    hyperparameter=\"max_samples\",\n",
    "    hyperparameter_values = range(100,6100,100),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the graph, we can see that although an initial increase in the maximum samples improves the predictions of the model on both the training and test data, these plateau off, so further increase is not beneficial. Also, notice that the line for the validation data plateaus before the training data suggesting the model may be overfitting to the training data in this region.\n",
    "\n",
    "## 3. Systematically optimising multiple hyperparameter values to obtain a final model\n",
    "\n",
    "In the previous section, we explored the effect of varying individual hyperparameter values. However, when developing a model, we likely wish to optimise multiple hyperparameters systematically and simultaneously. It is prudent to optimise hyperparameters simultaneously as they are not neccesarily independent of each other. For example, consider the maximum tree depth, `max_depth`, and minimum number of samples required to split a node, `min_sample_split`, hyperparameters. Let the value of `max_depth`, but also the value of `min_sample_split`, be relatively large. Then even though `max_depth` would allow for trees of large depth, the large value of `min_sample_split` may prevent some of the splits that would allow for trees with a large depth to occur.\n",
    "\n",
    "One possible method of varying multiple hyperparameters is by grid search. This exhausative method will iterate through every possible combination of multiple hyperparameters. For example, say a model has three hyperparameters A, B and C, and we sought to optimise over the following values of each hyperparameter:\n",
    "\n",
    "- `A = {a1, a2, a3}`\n",
    "- `B = {b1, b2}`\n",
    "- `C = {c1}`\n",
    "\n",
    "By undertaking a grid search, we would iterate through all possible combinations of these values to try and find the most optimal:\n",
    "\n",
    "- `A = a1, B = b1, C = c1`\n",
    "- `A = a1, B = b2, C = c1`\n",
    "- `A = a2, B = b1, C = c1`\n",
    "- `A = a2, B = b2, C = c1`\n",
    "- `A = a3, B = b1, C = c1`\n",
    "- `A = a3, B = b2, C = c1`\n",
    "\n",
    "Rather than assessing these combinations of hyperparameters on just one particular split of the training data, like we did in the previous section with `X_training`, `y_training`, `X_val` and `y_val`, here we will use cross-validation to generate many different splits of the data to assess the performance of our models on. In scikit-learn, we can specify a range of values for each hyperparameter we wish to simultaneously optimise using a parameter grid. Using the section 2.2 of this notebook, can you specify a range of values for the `max_depth` hyperparameter?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "param_grid = {\n",
    "    \"n_estimators\": range(5,55,5),\n",
    "    \"max_depth\": # Your code goes here\n",
    "    \"max_samples\": range(400,6800,400)\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "    <summary>Hint:</summary> One possible range for `max_depth` could be `range(5,30,5)`\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then use `GridSearchCV` with our specified `RandomForestClassifier` model and `param_grid`. Note, this may take a few minutes to run:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = RandomForestClassifier(random_state = 42)\n",
    "clf_cv = GridSearchCV(clf, param_grid)\n",
    "clf_cv.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can determine which combination of hyperparameter values were found to be most optimal as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf_cv.best_params_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also determine the accuracy from the mean cross-validation scores for the best choice of hyperparameters, which is returned using best_score_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf_cv.best_score_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recall, at the beginning of the notebook we isolated a subset of the data, `X_test` and `y_test`, so we could consider the generalisation ability of a final model with optimal hyperparameter values. We can now consider the accuracy of this model on this unseen data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf_cv.score(X_test, y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see this accuracy value is quite high and in line with other accuracy values suggesting our finalised model has generalised reasonably well to this unseen data.\n",
    "\n",
    "Throughout this notebook, the metric we have used to assess our potential models is accuracy. However, this is only one possible metric we could have used, and we could have even considered multiple. Good metric choice depends on the nature of our data and the domain it relates to.\n",
    "\n",
    "## 4. Extension to a support vector machine model\n",
    "\n",
    "As an optional extension activity, can you optimise the hyperparameters of a [support vector machine model](https://scikit-learn.org/stable/modules/svm.html)? \n",
    "\n",
    "Note, unlike random forest models, support vector machine models can be quite sensitive to the scaling of different features, so it is recommended to first scale the data. More details on how we can go about doing this can be found in [sections 10.1 and 10.2](https://scikit-learn.org/stable/common_pitfalls.html) of the scikit-learn documentation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Get worked example or other files\n",
    "\n",
    "Towards the end of the session, we will make a worked example available for you to have a look at if you would like. When we let you know this is ready, you can get a copy by running the cell below.\n",
    "\n",
    "If we should need to give you any other files, please paste the filename given into the bottom cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Run at end of session to get worked example\n",
    "! cp /mnt/materials/Hartree/ds/advancing_data_science/session3/hyperparameters_answers.ipynb ."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Paste filename where indicated and run cell if we need to give you any other files\n",
    "#(taking care to leave the final dot, separated by a space)\n",
    "! cp /mnt/materials/Hartree/FILENAME_HERE ."
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "cf6fe4ec1fa2d08ccf32805bda6ee12596beda46abe61d498e034c860f32f5db"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
