import numpy as np


def classification_report_to_table(
    str_representation_of_report, return_transpose_values=True
):
    split_string = [x.split(" ") for x in str_representation_of_report.split("\n")]
    column_names = [""] + [x for x in split_string[0] if x != ""]
    values = []
    for table_row in split_string[1:-1]:
        table_row = [value for value in table_row if value != ""]
        if table_row != []:
            values.append(table_row)
    for i in values:
        for j in range(len(i)):
            if i[1] == "avg":
                i[0:2] = [" ".join(i[0:2])]
            if len(i) == 3:
                # i.insert(1,np.nan)
                # i.insert(2, np.nan)
                i.insert(1, "")
                i.insert(2, "")
            else:
                pass
    if return_transpose_values is True:
        return np.transpose(values), column_names
    else:
        return values, column_names
