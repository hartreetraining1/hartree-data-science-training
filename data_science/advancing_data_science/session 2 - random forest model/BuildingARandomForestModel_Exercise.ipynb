{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "3b066c14-9d65-455d-887c-4cae71ca1d60",
   "metadata": {},
   "source": [
    "<img src=\"../../../shared/img/UKRI_STFC_HARTREE_CENTRE_RGB.png\" width=\"25%\" align=\"left\"><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "589f6afe-9ac1-4f29-b366-7f3a5534a0f6",
   "metadata": {},
   "source": [
    "### INTELLECTUAL PROPERTY RIGHTS NOTICE:\n",
    "The User may only download, make and retain a copy of the materials for their use for non-commercial and research purposes. If you intend to use the materials for secondary teaching purposes it is necessary first to obtain permission.\n",
    "The User may not commercially use the material, unless a prior written consent by the Licensor has been granted to do so. In any case, the user cannot remove, obscure or modify copyright notices, text acknowledging or other means of identification or disclaimers as they appear.\n",
    "For further details, please email us:  hartreetraining@stfc.ac.uk\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "foster-retailer",
   "metadata": {},
   "source": [
    "# Building a Machine Learning Model "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a015c19-4902-436e-87fc-2e232af3fc86",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\"> Notebook instructions:\n",
    "<ol>\n",
    "    <li>To edit a cell, double click on it.\n",
    "    <li>You can change a cell from markdown (text) to code using the drop down box at the top of this page.</li><br>\n",
    "        <img src=\"../../../shared/img/jupyter_header.png\" width=\"50%\" align=\"center\"><br><br>\n",
    "    <li>To run a cell, press Ctrl and Enter or the play icon in the toolbar at the top of this page.</li>\n",
    "    <li>Please ensure you run the import statements cell immediately below these instructions before beginning with any exercise.</li>\n",
    "    <li> If you make an error and have run your code, it is best to restart your notebook and run the cells again. Click the refresh icon in the jupyter toolbar and then 'Cell'>'Run all above' before running your corrected code again.</li>\n",
    "    <li>To add scroll bars to output, right click on the code cell which generated the output, and select \"Enable Scrolling for Outputs\" from the dropdown.</li>\n",
    "    </ol>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f91cc54d-6ee4-4f3c-bd6a-16ddd30e6781",
   "metadata": {},
   "source": [
    "## 1. Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "92d84e2b-7029-497a-a4ba-1b8d47836c3e",
   "metadata": {},
   "source": [
    "In this notebook, we will take you step-by-step through the process of creating a classification machine learning model for the **classification_data.csv** data that has been provided to you. This process will highlight the necessary steps needed to create and evaluate the model which has been created for your data set. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "98b0e55b-854f-4634-b3d9-e24ce347d130",
   "metadata": {},
   "source": [
    "#### Contents"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6b5aabe-0e4e-45e6-93b9-c2051f800935",
   "metadata": {},
   "source": [
    "The steps shown in this notebook are as follows:  \n",
    "- Installing and importing the necessary Python Libraries.\n",
    "- Importing our data using Pandas \n",
    "- Running simple checks on our data once it is imported\n",
    "- Simple Data Exploration and Visualisation\n",
    "- Splitting the data into Training, Testing and Validation datasets\n",
    "- Building a RandomForest model\n",
    "- Evaluating the model using different performance metrics"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55b6a437-5806-48d9-b91f-b9b660a1fefa",
   "metadata": {},
   "source": [
    "### Installation and Importing Libraries"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94c3b798-2c0b-441b-bf7c-211eabd322fd",
   "metadata": {},
   "source": [
    "#### Installation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72730372-3685-4eb2-b85a-34282a3c3761",
   "metadata": {},
   "source": [
    "The required Python modules have been preinstalled in this training environment for you."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05d2fbb3-5a08-482a-9fe1-2a1911f99b22",
   "metadata": {},
   "source": [
    "#### Importing the libraries into our notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "993f498e-ed75-4038-b173-d5ad3d96f326",
   "metadata": {},
   "source": [
    "Once the libraries are installed on our machine, we can import them into our notebook as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "pursuant-jacksonville",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Import the libraries required \n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "from sklearn.preprocessing import MinMaxScaler, StandardScaler\n",
    "from sklearn.model_selection import train_test_split, cross_validate\n",
    "from sklearn.ensemble import RandomForestClassifier\n",
    "from sklearn.metrics import accuracy_score, precision_score, recall_score, confusion_matrix\n",
    "from sklearn.metrics import classification_report, balanced_accuracy_score, f1_score\n",
    "from sklearn.svm import SVC\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cardiovascular-switzerland",
   "metadata": {},
   "source": [
    "Now that the libraries are installed, we can get to having a look at the data!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "232970a0-1e73-449a-8950-ecdbde9a3906",
   "metadata": {},
   "source": [
    "### Importing our data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d3fdfb2-eb26-4797-8a1f-8379aa0c2a17",
   "metadata": {},
   "source": [
    "We can import our data using the Pandas library we have just imported. As our data is in the csv file format, we can open it using the command:  \n",
    "- **df = pd.read_csv(file)**  \n",
    "\n",
    "\n",
    "We can then check if the data has been imported by viewing the first few rows. This can be done using the command: \n",
    "- **df.head()**  \n",
    "\n",
    "\n",
    "To import your data, try both commands in the cell below.  \n",
    "(Remember to change _'FILENAME.csv'_ to the actual name of our data file)\n",
    "\n",
    "The filename for the data is **classification_data.csv** (the rest of the path is already given for you)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "timely-dialogue",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Try the code in this cell\n",
    "#Replace the FILENAME with the correct filename\n",
    "df = pd.read_csv(\"/mnt/materials/Hartree/ds/data/lccg_data/FILENAME.csv\")\n",
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63bbc679-6699-4ce1-8a7a-87dface373d9",
   "metadata": {},
   "source": [
    "#### What does our data contain?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e60782ca-9b08-4196-87ff-c46b5fb614cd",
   "metadata": {},
   "source": [
    "  \n",
    "From the presentation slides, we can see that the data is organised as follows:  \n",
    "\n",
    "- Outcome – whether patient needed additional care. (0 – no, 1 - yes)\n",
    "- Age (in years, should all be 18+)\n",
    "- Prior referrals – whether the patient has had a previous referral for this type of care (0 – no, 1- yes)\n",
    "    - Prior Social Care referral\n",
    "    - Prior mental health referral\n",
    "    - Prior community care referral\n",
    "- Number of diagnoses – how many different prior diagnoses the patient has on their records\n",
    "- Number of prescriptions – How many different items have been prescribed for the patient in the past year.\n",
    "- Number of prior admissions – how many hospital emissions the patient has had in the past year.\n",
    "- Diagnoses – The remaining columns indicate whether a patient has a particular condition (0 – No, 1 – yes)\n",
    "    - Hypertension\n",
    "    - Palliative Care Register/ End of Life Care Register​\n",
    "    - Psychoactive Substance Misuse Disorder\n",
    "    - Poisoning\n",
    "    - Anxiety\n",
    "    - CKD\n",
    "    - Schizophrenia\n",
    "    - Dementia  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d728c98e-4c83-48db-b270-dd8bc6b648d5",
   "metadata": {},
   "source": [
    "#### What are we aiming to do?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2dde0ab1-f335-4efa-80e9-79f47d5af4c3",
   "metadata": {},
   "source": [
    "Using our data, we are aiming to create a model that can predict whether a patient will need additional care or not. Therefore, in terms of the data table, we are trying to predict whether the 'Outcome' column of a patients data should be a value of 0 (no additional care needed) or 1 (additional care is needed)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d239a798-134b-43bf-9830-866b67841148",
   "metadata": {},
   "source": [
    "### Checking our data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9446e6b9-4f01-493d-9fa2-fd163d71aae6",
   "metadata": {},
   "source": [
    "Now that we have imported our data, and understood what each column contains, we need to check the data for any inaccuracies."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aac270ad-f1f7-419c-8eab-67bf63af0fd0",
   "metadata": {},
   "source": [
    "#### Column Names"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8292c840-904a-4e45-bdf9-8b112424ca46",
   "metadata": {},
   "source": [
    "The first simple check that we can do is to print the names of the columns and make sure that they match up with the list of columns above.  \n",
    "We can do this using the command:  \n",
    "- **print(df.columns.values)**\n",
    "    \n",
    "Try it yourself in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "enclosed-geometry",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Try the command in this cell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba9244a2-0045-470a-9784-e261cfcbf025",
   "metadata": {},
   "source": [
    "By doing this, we should see a list of columns, which should match up with the descriptions of the columns above. If so, we have completed the check that all the columns are present.  \n",
    "\n",
    "Next, we need to check for missing values."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4e3875c-59c0-4a42-9218-45d99391b98e",
   "metadata": {},
   "source": [
    "#### Missing Values"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef91b590-b5ea-4dba-93ea-994516dbbe47",
   "metadata": {},
   "source": [
    "  \n",
    "\n",
    "In order to check that every cell within the dataset contains a real value, we can use the command:  \n",
    "- **print(df.isnull().sum())**  \n",
    "\n",
    "This command will list the number of missing values (either Null or NaN values) in each column. If the number associated with the column is non-zero, there will be missing values in that column.  \n",
    "Try this command yourself in the cell below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "charitable-waters",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Try the command in this cell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7967fc05-eeb9-4eb3-ad38-f336b1f33e5d",
   "metadata": {},
   "source": [
    "### Data Exploration"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b1acb56-3073-435b-803e-877fbd8d40f4",
   "metadata": {},
   "source": [
    "Now that we have checked that our data has no missing columns and no missing values, we need to check that the values within it are behaving as we would expect them to behave.  \n",
    "\n",
    "Pandas has a useful command which allows us to describe the summary statistics of each column within the dataset, which can give us a better insight into whether the data has any errors or anomalous values within it.  \n",
    "\n",
    "This is done using the command:  \n",
    "- **df.describe()**\n",
    "\n",
    "This prints a full summary statistics for every column in the dataset. Try it yourself below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "speaking-cotton",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Try the command in the cell below"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "brazilian-lancaster",
   "metadata": {},
   "source": [
    "This method also allows us to print out the summary statistics of a specific column. For example:  \n",
    "- **df['Age'].describe()**  \n",
    "\n",
    "This will only print the summary statistics for the 'Age' column.\n",
    "\n",
    "Try to print the summary statistics for the 'Outcome' column in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "greek-indonesian",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Print the 'Outcome' summary statistics in this cell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "streaming-license",
   "metadata": {},
   "source": [
    "We know from the description of the dataset that the 'Outcome' column contains only 0's and 1's. So we can use the _min_ and _max_ values above to check whether there are any erroneous values outside the range of 0 and 1.  \n",
    "We can also see that the _mean_ is closer to 0 than 1. This could mean that either the dataset contains slightly unbalanced data or there could be rogue decimal values hidden in the column which are bringing the _mean_ down. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "middle-grammar",
   "metadata": {},
   "source": [
    "We can use the command below to print the number of values which contain either 0 or 1. The isin() function simply tells us if the values are in the list of accepted values which we have provided. The count() function counts the number of times this is True. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "innovative-insight",
   "metadata": {},
   "outputs": [],
   "source": [
    "accepted_values = [0,1]\n",
    "df['Outcome'].isin(accepted_values).count()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "unauthorized-capital",
   "metadata": {},
   "source": [
    "We can then print the total number of values in the column to check that they match."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "urban-equivalent",
   "metadata": {},
   "outputs": [],
   "source": [
    "len(df['Outcome']) "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "80ecc99b-069a-4779-a353-71ed906a160b",
   "metadata": {},
   "source": [
    "From this, we can see that all our values in that column are accepted values of 0 or 1."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81a1cd5b-367b-41a3-843a-3e4c2e0d1cde",
   "metadata": {},
   "source": [
    "### Data Visualisation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db68e909-1c57-4660-a3c4-f112a1a694cb",
   "metadata": {},
   "source": [
    "Another great way of checking your data is to create plots which visualise different features of the data. If we consider our previous problem, we suggested that either the data contained decimal values... or was unbalanced! So now, lets use a bar plot to check whether the Outcome section of the data is balanced or not. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "assigned-riverside",
   "metadata": {},
   "outputs": [],
   "source": [
    "#This code counts the number of occurences of 0 and 1 within the 'Outcome' column\n",
    "add_care_no = df['Outcome'][df['Outcome']==0].count()\n",
    "add_care_yes = df['Outcome'][df['Outcome']==1].count()\n",
    "#Next, list our column names, and create a list of values associated with each Outcome\n",
    "columns = ['No','Yes']\n",
    "values = [add_care_no, add_care_yes]\n",
    "\n",
    "#Use matplotlib.pyplot to create a bar chart\n",
    "plt.bar(columns,values)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "understanding-chick",
   "metadata": {},
   "source": [
    "As we can see, there are much more occurences where no additional care is needed than where it is needed. This would explain the _mean_ value being closer to 0 than 1. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "valid-technology",
   "metadata": {},
   "source": [
    "We can also create histograms that allow us to visualise the distribution of values within a dataset.  \n",
    "For example, for the 'Age' data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "hydraulic-ticket",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(df['Age'], bins=10)\n",
    "plt.title('Histogram of Age')\n",
    "plt.xlabel('Age')\n",
    "plt.ylabel('Count')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "second-synthesis",
   "metadata": {},
   "source": [
    "Try yourself to create a histogram of the 'Number of Prior Admissions' column in the cell below. The plot should contain 20 bins, and have an appropriate title and axis labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "basic-office",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Create the histogram in this cell."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "impressed-search",
   "metadata": {},
   "source": [
    "Now that we have checked and explored our data, we can get to building the Machine Learning model!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "historical-rover",
   "metadata": {},
   "source": [
    "## 2. Building the Random Forest Classifer Model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "duplicate-touch",
   "metadata": {},
   "source": [
    "To begin with, we need to identify - using our aims - which columns we want to use as features which train the model, and which column we would like to predict.  \n",
    "As the aim of the model is to predict whether a patient will need additional care, the 'Outcome' column is what we want to predict, and the other diagnoses columns will be used as features to predict the 'Outcome'.\n",
    "\n",
    "Therefore the first step is to split these columns into separate dataframes. We will call these 'x' and 'y'. Where the 'x' dataframe contains the features and the 'y' dataframe contains the predictor: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "early-protocol",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Name the column we want to predict. \n",
    "y = df['Outcome']\n",
    "\n",
    "#As all the other columns are features, we want to create a df which is a copy of df, but\n",
    "#doesnt contain the 'Outcome' column.\n",
    "# A simple way to do this is to copy the full df, and then drop the Outcome column.\n",
    "\n",
    "x = df.copy() #copies the original df into new variable y\n",
    "x = x.drop(columns=['Outcome']) #removes the 'Outcome' column\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "excited-individual",
   "metadata": {},
   "source": [
    "The next step is to Standardize the values in the features column. We do this because this means that each feature has the same range of values and therefore this allows relationships between features to be observed.  \n",
    "\n",
    "We can do this using the StandardScaler() from the sklearn package (we have already imported this at the start!)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "standard-federation",
   "metadata": {},
   "outputs": [],
   "source": [
    "scaler = StandardScaler() #This command initialises the scaler\n",
    "x = scaler.fit_transform(x.to_numpy())\n",
    "#This converts the x dataframe to a numpy series (ie. x.to_numpy()) and then scales it. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "coupled-hampshire",
   "metadata": {},
   "source": [
    "### Creating a Training, Validation and Test Set"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dirty-dealer",
   "metadata": {},
   "source": [
    "Next, we want to split our data into Training, Validation and Test sets. We do this as each set of data will have a different use within our model: \n",
    "- Training Set:\n",
    "    - This is the data that our model will see and attempt to learn from. \n",
    "- Validation Set: \n",
    "    - This data is not seen by the model. When creating our model we use this to test our model against perfomance metrics. This allows us to change the hyperparamters of our model in order to optimize the performance before testing further on unseen data. \n",
    "- Test Set:\n",
    "    - This is data that is once again unseen by either the model or the validation testing. Once we are happy with our validation performance we use this to test the model on independant, unseen data. The performace of the model on this data can be used to tell the world how well our model will perform in real life.  \n",
    "    \n",
    "**_sklearn_** includes a function which can be used to split the data into two:  \n",
    "- **train_test_split()**\n",
    "\n",
    "\n",
    "If we use this function twice, once on the full dataset and again on one of the two created, we can split the data into three.  \n",
    "\n",
    "So firstly, we will split the data into two, a training and a test set. The test set typically contains 10% of the data within the full data set. Also remember, we need to do this for both the feature (x) and predictor (y) columns "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "existing-asbestos",
   "metadata": {},
   "outputs": [],
   "source": [
    "x_train, x_test, y_train, y_test = train_test_split(x,y, test_size=(0.10))\n",
    "#Here we specify the datasets to be split (x and y),and the size of the test set"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "valued-spare",
   "metadata": {},
   "source": [
    "This creates a train and test set for both the feature and predictor columns.  \n",
    "Next we want to further split the training set into training and validation sets (where the validation set is 1/3 the size of the data. We can do this by inputting our x_train, y_train values into the function instead of x and y.  \n",
    "\n",
    "Try it yourself below, and call the resulting training sets: x_train, x_val, y_train, y_val.  \n",
    "Remember to change the test size as well!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "velvet-windsor",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Further split the data into training and validation in this cell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "natural-investor",
   "metadata": {},
   "source": [
    "We can check this has worked correctly by printing the lengths of each set (for either x or y). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eastern-helicopter",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'Size of training set: {len(x_train)}')\n",
    "print(f'Size of validation set: {len(x_val)}')\n",
    "print(f'Size of test set: {len(x_test)}')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "retained-monkey",
   "metadata": {},
   "source": [
    "We can also use the Cross Validation method. This is particularly useful for tuning hyperparameters of the model, and will be covered in the 'Tuning Hyperparameters' section of the course."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "suspected-exchange",
   "metadata": {},
   "source": [
    "### Creating the Model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "arbitrary-kitchen",
   "metadata": {},
   "source": [
    "Next, we are getting to creating the model which will help us predict whether a patient will need additional care or not. In this case we are using a RandomForest model which will classify a patient as 'Outcome' = 0 or 'Outcome' = 1, i.e. will the patient need additional care? "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "greater-concept",
   "metadata": {},
   "source": [
    "The first step is to initialise the RandomForestClassifier and then tell the function to use our training set data to develop a model. The most basic way to do this is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "respected-slope",
   "metadata": {},
   "outputs": [],
   "source": [
    "rf_model = RandomForestClassifier() #this initilialises the classifier\n",
    "rf_model.fit(x_train, y_train) # use the training set to build a model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "indonesian-response",
   "metadata": {},
   "source": [
    "Next, we use the model to predict the outcomes of patients within the validation set. This will create predictions for each patient which we can then compare to the actual outcome to measure the performance of our model.  \n",
    "\n",
    "In order to create the predictions, we use: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "periodic-japanese",
   "metadata": {},
   "outputs": [],
   "source": [
    "rf_y_val_pred = rf_model.predict(x_val) #creates prediction of outcomes for validation set"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "colored-blank",
   "metadata": {},
   "source": [
    "We also need to do this for the training set in order to analyse performance of the model on the training set itself. In the cell below, create a variable called _**rf_y_train_pred**_ which creates predictions for the values in the training set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "metropolitan-angel",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Create the predictions for the training set in this cell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a1700221-bb8c-438d-bd99-faef1d934a3e",
   "metadata": {},
   "source": [
    "This has now created a set of predictions which we can compare to the real answers to measure performance.  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d778cb1f-3d66-466b-98f1-5bdcb0db9799",
   "metadata": {},
   "source": [
    "### Measuring Performance"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "317caa3b-625e-4701-b462-d4f847067379",
   "metadata": {},
   "source": [
    "  \n",
    "\n",
    "From the slides we can see that there are many different metrics by which we can measure the performance of the model we have created. Many of these metrics are based on the confusion matrix.  \n",
    "\n",
    "In the cell below, I have created a confusion matrix of the validation set predictions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "wrapped-lawrence",
   "metadata": {},
   "outputs": [],
   "source": [
    "tn, fp, fn, tp = confusion_matrix(y_val, rf_y_val_pred).ravel()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "extra-scotland",
   "metadata": {},
   "source": [
    "Where:\n",
    "- tn : true-negative\n",
    "- fp : false-positive\n",
    "- fn : false-negative\n",
    "- tp : true-positive  \n",
    "\n",
    "Using these variables, attempt to calculate and print the scores for: \n",
    "- $Accuracy = \\frac{TP + TN}{TP + FN + FP + TN}$  \n",
    "\n",
    "- $Recall = \\frac{TP}{TP + FN}$  (this is also Sensitivity)\n",
    "\n",
    "- $Precision = \\frac{TP}{TP + FP}$  \n",
    "\n",
    "- $f1 score = 2 \\times \\frac{precision \\times recall}{precision + recall}$  \n",
    "\n",
    "- $Specificity = \\frac{TN}{TN + FP}$  \n",
    "\n",
    "for this model, using the equations which can be found on the slides. Try it in the cell below: \n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "emerging-projection",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Calculate the performance metrics here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "flying-boating",
   "metadata": {},
   "source": [
    "This is useful for remembering the underlying equations behind the performance metrics. \n",
    "However, if you cant, luckily **sklearn** has many of these performance metrics built in! "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "rising-agriculture",
   "metadata": {},
   "source": [
    "For example, to calculate the accuracy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "oriented-arcade",
   "metadata": {},
   "outputs": [],
   "source": [
    "accuracy_score(y_val, rf_y_val_pred)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "caring-occasion",
   "metadata": {},
   "source": [
    "The built in performance metrics are:\n",
    "- accuracy_score\n",
    "- balanced_accuracy_score (for unbalanced data)\n",
    "- precision_score\n",
    "- recall_score\n",
    "- f1_score\n",
    "\n",
    "These are all used within the code as they are named in the list above, and all have the same input (ie. set of true values, set of predictions). \n",
    "\n",
    "As such, try to calculate the scores again but using the built-in functions listed above.  \n",
    "\n",
    "Try this in the cell below (using the accuracy_score we have just calculated as a hint!)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "moral-mystery",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Calculate and print the perfomance metrics above in this cell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "smooth-robin",
   "metadata": {},
   "source": [
    "It's also a good idea to measure the performance of the model on the data it was trained on. \n",
    "In order to do this we need to input the training values and the predicted training values into the functions listed above. Try this in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "driven-amendment",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Calculate and print the perfomance metrics for the training data in this cell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cultural-missile",
   "metadata": {},
   "source": [
    "### Optimising Model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "tutorial-raising",
   "metadata": {},
   "source": [
    "Now that we have our initial scores that how well the model performs on the validation data, we want to adjust the hyperparameters of the model in order to optimize the performance of the model. This will be covered in the next exercise. \n",
    "\n",
    "For our purposes, we shall pretend that we have optimised the performance on the validation set to a standard that we are happy with! "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c009c508-c0d6-4e7f-9608-1e01a92b50a9",
   "metadata": {},
   "source": [
    "### Testing"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "131cb8b6-5925-476d-92cc-c97e771ad895",
   "metadata": {},
   "source": [
    "Our final stage of the process is to test the model on the test set that we set aside earlier. This data has been kepy completely independant from the model training and model validation steps, and therefore will provide the final metric on how well the model performs on 'real life' data.  \n",
    "\n",
    "The first step, is to predict the outcome values for the features found in the test set. We previously did this for the validation set in cell no. 19. So in the cell below, try to create the predictions for the test set instead. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "divided-davis",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Create test set predictions here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "temporal-hopkins",
   "metadata": {},
   "source": [
    "This has created the 'Outcome' predictions for the test set. Next, we need to measure the final performance metrics.  \n",
    "\n",
    "For example: \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "focal-oracle",
   "metadata": {},
   "outputs": [],
   "source": [
    "accuracy_score(y_test, rf_y_test_pred)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "following-legislation",
   "metadata": {},
   "source": [
    "Using the test set data and predictions, calculate the other built in performance metrics in the cell below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "conceptual-punishment",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Calculate and print performance metrics for test set"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "painted-involvement",
   "metadata": {},
   "source": [
    "Well done! You have completed all the exercises and successfully built and tested a simple machine learning model.  \n",
    "\n",
    "If you have time there is a bonus exercise below which uses a Support Vector Machine model rather than Random Forest. If you complete both it is a good idea to compare the performance result that each have acheived. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "defensive-screw",
   "metadata": {},
   "source": [
    "## Bonus Exercise: Support Vector Machines"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "overall-river",
   "metadata": {},
   "source": [
    "Many of the steps needed to complete the Random Forest model are repeated to create a model using a Support Vector Machine. \n",
    "In this task, the steps needed will be highlighted in the comments of each cell, but there will not be code examples for each step, as these can be found above in the RandomForest example.  \n",
    "\n",
    "The answers will be available in the answers notebook. \n",
    "\n",
    "Hints: \n",
    "- Many of the steps are the same as the Random Forest model, but variable names will need to be changed. \n",
    "- The data has already been split into training, validation and test sets. So you can just use the variable names we have previously used.  \n",
    "\n",
    "To initialise the SVM model, run the code below: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "relative-salad",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.svm import SVC\n",
    "svc_model = SVC()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "handled-stick",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Using the training data, fit the model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "adaptive-shade",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Using the validation data, create predictions for the 'Outcome' column"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "seventh-saudi",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Calculate the built-in performance metrics for the SVM model on the validation set"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "prospective-literacy",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Using the training data, create predictions for the 'Outcome' column\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "brief-perception",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Calculate the built-in performance metrics for the SVM model on the training set\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "embedded-mongolia",
   "metadata": {},
   "outputs": [],
   "source": [
    "##Using the test data, create predictions for the 'Outcome' column\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "wrapped-differential",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Calculate the built-in performance metrics for the SVM model on the test set\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "charitable-malta",
   "metadata": {},
   "source": [
    "Using the performance metrics for the test sets. Which model do you think performed best?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "competent-jumping",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "10204b7d-70ea-4202-a428-016d229cb39b",
   "metadata": {},
   "source": [
    "# Get worked example or other files\n",
    "\n",
    "Towards the end of the session, we will make a worked example available for you to have a look at if you would like. When we let you know this is ready, you can get a copy by running the cell below.\n",
    "\n",
    "If we should need to give you any other files, please paste the filename given into the bottom cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c1b7f577-8d77-4ccc-9443-83d4f97a2a57",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Run at end of session to get worked example\n",
    "! cp /mnt/materials/Hartree/ds/advancing_data_science/session2/BuildingARandomForestModel_Exercise-Answers.ipynb ."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aa4b80d6-1443-402c-8438-d4789579ebe0",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Paste filename where indicated and run cell if we need to give you any other files\n",
    "#(taking care to leave the final dot, separated by a space)\n",
    "! cp /mnt/materials/Hartree/FILENAME_HERE ."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0fa8da08-5d6e-4615-8a06-f1303eae31cd",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
